require 'socket'
class AdsController < ApplicationController
  before_action :set_ad, only: [:toggle, :pictures, :edit, :update, :destroy, :increase, :pay]
  before_action :set_ad_pams, only: [:show]
  before_action :authenticate_user!, except: [:show, :index, :increase,:setup, :by_zone, :cities]
  # GET /ads
  # GET /ads.json
  #
  #


  def pay
  end

  def toggle
      @ad.update_attributes({
          :active => (@ad.active ? 0 : 1)
        })
      redirect_to "/annunci/admin?page=#{session[:pg]}"
  end

  def set_ads_order
    session[:order] = params[:order]
    redirect_to "/"
  end

  def increase
      @ad.update_attributes({
            :views_count => (@ad.views_count || 0) + 1
        })
        respond_to do |format|
          format.json { render json: @ad, status: :ok }
        end
  end

  def pictures
    respond_to do |format|
      format.json { render json: @ad.pictures.to_json({
          :methods => [:cropped]
        }), status: :ok }
    end
  end


  def by_zone

    ads = Ad.all.group("base")

    respond_to do |format|
      format.json { render json: ads.as_json({
          :only => [:base],
          :methods => [:amount]
        }), status: :ok }
    end

  end



  def admin
    if current_user.user_type == "A"
      @q = Ad.all.ransack(params[:q])
      @qq = User.all.ransack(params[:q])
    else
      redirect_to current_user
    end
  end

  def setup
    Ad.all.each do |ad|
      ad.update_attributes({
          :title => ad.user.username
        })
        Ad.find_each(&:save)
    end
    redirect_to "/"
  end

  def cities
  end

  def index

    begin

    # GeoIp.api_key = '04fd269e2098c82c903631387c552d99ff33dd38dde2b5446ba1169d6403aaef'

    if !current_user
    else
        if !current_user.username && current_user.user_type == "U"
          redirect_to infos_users_path
        elsif current_user.user_type == "A"
          redirect_to admin_ads_path
        end
    end
    @ad_categories = AdCategory.all
    # if params[:q]
    #   bearing = params[:q][:address_cont]
    #   params[:q].delete :address_cont
    # end
    #


    if !params[:cq]

    if(!params[:city])
      @q = Ad.where({
          :active => 1
        }).ransack(params[:q])
    else
      if params[:q]
        params[:q].delete :address_cont
      end
      @q = Ad.where({
          :active => 1
        })
        .where("base like ? or secondary_base like ?", "%#{params[:city]}%","%#{params[:city]}%")
        .ransack(params[:q])
    end

    if(!params[:category])
      @q = Ad.where({
          :active => 1
        }).ransack(params[:q])
    else
      if params[:q]
        params[:q].delete :ad_category_id_eq
      end
      cat = AdCategory.find_by_name(params[:category])
      if(params[:city])
      @q = Ad.where({
          :active => 1
        }).where("base like ? or secondary_base like ?", "%#{params[:city]}%","%#{params[:city]}%").where({
          :ad_category_id => cat.id
        }).ransack(params[:q])
      else
        @q = Ad.where({
            :active => 1
          }).where({
            :ad_category_id => cat.id
          }).ransack(params[:q])
      end


   



    end


          if(params[:around])
            @q = Ad.where({
                :active => 1
              }).near(params[:around].split(","), params[:radar]).ransack(params[:q])
          end


    else
      cq = CustomQuery.where("path like ?", "%#{params[:cq]}%").first
      _q = Ad.where({
          :active => 1
        }).joins(:pictures).group(:id).where(cq.query)
      if cq.order
        _q = _q.order(cq.order)
      end
      p _q.to_sql
      @q = _q.ransack(params[:q])
      # @q = Ad.where(cq.first.query)
    end
    @ads = @q.result(distinct: true).page(params[:page]).per(24)
    if !params[:page]
    @ads = @ads.order("RAND()")
  end

    if params[:city]
        d = Default.find_by_field("home_"+params[:city].downcase)
        if d
           _v = d.value;
        else
          _v = Default.find_by_field("home_generico").value;
        end

    else
      _v = Default.find_by_field("home_generico").value;
    end

    begin
     @title = _v.gsub!("{{category}}", params[:category].capitalize).gsub!("{{city}}", params[:city].capitalize).html_safe
   rescue
      @title =  _v.html_safe

   end

    rescue
      ActiveRecord::Base.connection.execute("set global sql_mode='STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';")
      ActiveRecord::Base.connection.execute("set session sql_mode='STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';")
      redirect_to "/"
    end

  end


  # GET /ads/1
  # GET /ads/1.json
  def show
    if current_user
      @is_fav = current_user.favs.find_by_ad_id(@ad.id)
    else
      @is_fav = false
    end
    @fav = Fav.new


    @comment = Comment.new

    @ads = Ad.where({
        :ad_category_id => @ad.ad_category_id
      }).where("id != ?", @ad.id).near([@ad.latitude, @ad.longitude], 100).limit(6)

  end

  # GET /ads/new
  def new


    if current_user.ads.count == 0
      _ad = Ad.new({
          :user_id => current_user.id
        })
      _ad.save
      redirect_to profile_users_path
    else
      if current_user.ads.first.breakpoint_matches.count == 0
        redirect_to profile_users_path
      else
        if !current_user.ads.first.ad_category_id
          if(current_user.properties.count == 0)
            redirect_to properties_users_path
          else
            redirect_to edit_ad_path(current_user.ads.last)
          end
        else
          _ad = Ad.new({
              :user_id => current_user.id
            })
          _ad.save
          redirect_to profile_users_path
        end
      end
    end
    @ad = Ad.new
  end

  # GET /ads/1/edit
  def edit

    if !current_user.username
      flash[:notice] = 'Inserisci prima le tue informazioni, quali nome e numero di telefono, grazie!'
      redirect_to current_user
    end
  end

  # POST /ads
  # POST /ads.json
  def create
    @ad = Ad.new(ad_params)


    respond_to do |format|
      if @ad.save
        set_ad_query_params(@ad)

        format.html { redirect_to @ad, notice: 'Ad was successfully created.' }
        format.json { render :show, status: :created, location: @ad }
      else
        format.html { render :new }
        format.json { render json: @ad.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /ads/1
  # PATCH/PUT /ads/1.json
  def update
      respond_to do |format|
      if @ad.update(ad_params)

        if params[:ad][:pictures]

        params[:ad][:pictures].each do |a|
          _picture = Picture.new({
              :ad_id => @ad.id,
              :attachment => a
            })

          _picture.save!
        end
        end

        set_ad_query_params(@ad)

        format.html { redirect_to edit_ad_path(@ad), notice: 'Ad was successfully updated.' }
        format.json { render :show, status: :ok, location: @ad }
      else
        format.html { render :edit }
        format.json { render json: @ad.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ads/1
  # DELETE /ads/1.json
  def destroy
    sql = "SET FOREIGN_KEY_CHECKS = 0;"
    records_array = ActiveRecord::Base.connection.execute(sql)

    @ad.destroy

    respond_to do |format|
      format.html { redirect_to "/annunci/admin?page=#{session[:pg]}", notice: 'Ad was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ad

      @ad = Ad.friendly.find(params[:id])
      if !@ad
        @ad = Ad.find(params[:id])
      end

    end

    def set_ad_pams

      @ad = Ad.friendly.find(params[:id])


    end

    def set_ad_query_params ad
      qs = []
      ad.breakpoint_matches.each do |bm|
        qs << bm.breakpoint.name
      end

      ad.update_attributes({
          :queries => qs.map(&:inspect).join(', ')
        })


    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ad_params
      params.require(:ad).permit(:abstract,:notes,:active,:evidence,:no_sms_no_private,:secondary_base,:always_open,:mhf,:mht,:thf,:tht,:whf,:wht,:tdhf,:tdht,:fhf,:fht,:shf,:sht,:sshf,:ssht,:base, :zone,:views_count,:new_photo,:slug,:title,:hour,:pictures, :in_tour, :from, :to,:user_id, :ad_category_id, :address, :latitude, :longitude, :description, :views_count, :queries)
    end
end
