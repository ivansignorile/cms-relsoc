class UbiquitiesController < ApplicationController
  before_action :set_ubiquity, only: [:show, :edit, :update, :destroy]

  # GET /ubiquities
  # GET /ubiquities.json
  def index
    @ubiquities = Ubiquity.all
  end

  # GET /ubiquities/1
  # GET /ubiquities/1.json
  def show
  end

  # GET /ubiquities/new
  def new
    @ubiquity = Ubiquity.new
  end

  # GET /ubiquities/1/edit
  def edit
  end

  # POST /ubiquities
  # POST /ubiquities.json
  def create
    @ubiquity = Ubiquity.new(ubiquity_params)

    respond_to do |format|
      if @ubiquity.save
        format.html { redirect_to @ubiquity, notice: 'Ubiquity was successfully created.' }
        format.json { render :show, status: :created, location: @ubiquity }
      else
        format.html { render :new }
        format.json { render json: @ubiquity.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /ubiquities/1
  # PATCH/PUT /ubiquities/1.json
  def update
    respond_to do |format|
      if @ubiquity.update(ubiquity_params)
        format.html { redirect_to @ubiquity, notice: 'Ubiquity was successfully updated.' }
        format.json { render :show, status: :ok, location: @ubiquity }
      else
        format.html { render :edit }
        format.json { render json: @ubiquity.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ubiquities/1
  # DELETE /ubiquities/1.json
  def destroy
    ad = @ubiquity.ad
    @ubiquity.destroy
    respond_to do |format|
      format.html { redirect_to edit_ad_path(ad), notice: 'Ubiquity was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ubiquity
      @ubiquity = Ubiquity.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ubiquity_params
      params.require(:ubiquity).permit(:ad_id, :from, :to, :address, :latitude, :longitude)
    end
end
