class BreakpointMatchesController < ApplicationController
  before_action :set_breakpoint_match, only: [:show, :edit, :update, :destroy]

  # GET /breakpoint_matches
  # GET /breakpoint_matches.json
  def index
    @breakpoint_matches = BreakpointMatch.all
  end

  # GET /breakpoint_matches/1
  # GET /breakpoint_matches/1.json
  def show
  end

  # GET /breakpoint_matches/new
  def new
    @breakpoint_match = BreakpointMatch.new
  end

  # GET /breakpoint_matches/1/edit
  def edit
  end

  # POST /breakpoint_matches
  # POST /breakpoint_matches.json
  def create
    @breakpoint_match = BreakpointMatch.new(breakpoint_match_params)

    respond_to do |format|
      if @breakpoint_match.save
        format.html { redirect_to @breakpoint_match, notice: 'Breakpoint match was successfully created.' }
        format.json { render :show, status: :created, location: @breakpoint_match }
      else
        format.html { render :new }
        format.json { render json: @breakpoint_match.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /breakpoint_matches/1
  # PATCH/PUT /breakpoint_matches/1.json
  def update
    respond_to do |format|
      if @breakpoint_match.update(breakpoint_match_params)
        format.html { redirect_to @breakpoint_match, notice: 'Breakpoint match was successfully updated.' }
        format.json { render :show, status: :ok, location: @breakpoint_match }
      else
        format.html { render :edit }
        format.json { render json: @breakpoint_match.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /breakpoint_matches/1
  # DELETE /breakpoint_matches/1.json
  def destroy
    @breakpoint_match.destroy
    respond_to do |format|
      format.html { redirect_to breakpoint_matches_url, notice: 'Breakpoint match was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_breakpoint_match
      @breakpoint_match = BreakpointMatch.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def breakpoint_match_params
      params.require(:breakpoint_match).permit(:breakpoint_id, :ad_id, :breakpoint_category_id)
    end
end
