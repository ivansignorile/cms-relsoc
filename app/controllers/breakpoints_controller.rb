class BreakpointsController < ApplicationController
  before_action :set_breakpoint, only: [:show, :edit, :update, :destroy]

  # GET /breakpoints
  # GET /breakpoints.json
  def index
    @breakpoints = Breakpoint.all
  end

  # GET /breakpoints/1
  # GET /breakpoints/1.json
  def show
  end

  # GET /breakpoints/new
  def new
    @breakpoint = Breakpoint.new
  end

  # GET /breakpoints/1/edit
  def edit
  end

  # POST /breakpoints
  # POST /breakpoints.json
  def create
    @breakpoint = Breakpoint.new(breakpoint_params)

    respond_to do |format|
      if @breakpoint.save
        format.html { redirect_to "/", notice: 'Breakpoint was successfully created.' }
        format.json { render :show, status: :created, location: @breakpoint }
      else
        format.html { render :new }
        format.json { render json: @breakpoint.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /breakpoints/1
  # PATCH/PUT /breakpoints/1.json
  def update
    respond_to do |format|
      if @breakpoint.update(breakpoint_params)
        format.html { redirect_to "/", notice: 'Breakpoint was successfully updated.' }
        format.json { render :show, status: :ok, location: @breakpoint }
      else
        format.html { render :edit }
        format.json { render json: @breakpoint.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /breakpoints/1
  # DELETE /breakpoints/1.json
  def destroy
    @breakpoint.destroy
    respond_to do |format|
      format.html { redirect_to "/", notice: 'Breakpoint was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_breakpoint
      @breakpoint = Breakpoint.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def breakpoint_params
      params.require(:breakpoint).permit(:breakpoint_category_id, :name)
    end
end
