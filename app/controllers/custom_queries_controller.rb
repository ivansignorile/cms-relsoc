class CustomQueriesController < ApplicationController
  before_action :set_custom_query, only: [:show, :edit, :update, :destroy]

  # GET /custom_queries
  # GET /custom_queries.json
  def index
    @custom_queries = CustomQuery.all
  end

  # GET /custom_queries
  # GET /custom_queries.json
  def all
    @custom_queries = CustomQuery.all
  end

  # GET /custom_queries/1
  # GET /custom_queries/1.json
  def show
  end

  # GET /custom_queries/new
  def new
    @custom_query = CustomQuery.new
  end

  # GET /custom_queries/1/edit
  def edit
  end

  # POST /custom_queries
  # POST /custom_queries.json
  def create
    @custom_query = CustomQuery.new(custom_query_params)

    respond_to do |format|
      if @custom_query.save
        format.html { redirect_to "/", notice: 'Custom query was successfully created.' }
        format.json { render :show, status: :created, location: @custom_query }
      else
        format.html { render :new }
        format.json { render json: @custom_query.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /custom_queries/1
  # PATCH/PUT /custom_queries/1.json
  def update
    respond_to do |format|
      if @custom_query.update(custom_query_params)
        format.html { redirect_to "/", notice: 'Custom query was successfully updated.' }
        format.json { render :show, status: :ok, location: @custom_query }
      else
        format.html { render :edit }
        format.json { render json: @custom_query.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /custom_queries/1
  # DELETE /custom_queries/1.json
  def destroy
    @custom_query.destroy
    respond_to do |format|
      format.html { redirect_to custom_queries_url, notice: 'Custom query was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_custom_query
      @custom_query = CustomQuery.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def custom_query_params
      params.require(:custom_query).permit(:title, :path, :query, :order)
    end
end
