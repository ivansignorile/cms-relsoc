class DefaultsController < ApplicationController
  before_action :set_default, only: [:show, :edit, :update, :destroy]

  # GET /defaults
  # GET /defaults.json
  def index
    @defaults = Default.all
  end

  def all
    @defaults = Default.all
  end

  # GET /defaults/1
  # GET /defaults/1.json
  def show
  end

  # GET /defaults/new
  def new
    @default = Default.new
  end

  # GET /defaults/1/edit
  def edit
  end

  # POST /defaults
  # POST /defaults.json
  def create
    @default = Default.new(default_params)

    respond_to do |format|
      if @default.save
        format.html { redirect_to "/", notice: 'Default was successfully created.' }
        format.json { render :show, status: :created, location: @default }
      else
        format.html { render :new }
        format.json { render json: @default.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /defaults/1
  # PATCH/PUT /defaults/1.json
  def update
    respond_to do |format|
      if @default.update(default_params)
        format.html { redirect_to "/", notice: 'Default was successfully updated.' }
        format.json { render :show, status: :ok, location: @default }
      else
        format.html { render :edit }
        format.json { render json: @default.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /defaults/1
  # DELETE /defaults/1.json
  def destroy
    @default.destroy
    respond_to do |format|
      format.html { redirect_to defaults_url, notice: 'Default was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_default
      @default = Default.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def default_params
      params.require(:default).permit(:field, :value)
    end
end
