class BreakpointCategoriesController < ApplicationController
  before_action :set_breakpoint_category, only: [:show, :edit, :update, :destroy]

  # GET /breakpoint_categories
  # GET /breakpoint_categories.json
  def index
    @breakpoint_categories = BreakpointCategory.all
  end

  # GET /breakpoint_categories/1
  # GET /breakpoint_categories/1.json
  def show
  end

  # GET /breakpoint_categories/new
  def new
    @breakpoint_category = BreakpointCategory.new
  end

  # GET /breakpoint_categories/1/edit
  def edit
  end

  # POST /breakpoint_categories
  # POST /breakpoint_categories.json
  def create
    @breakpoint_category = BreakpointCategory.new(breakpoint_category_params)

    respond_to do |format|
      if @breakpoint_category.save
        format.html { redirect_to "/", notice: 'Breakpoint category was successfully created.' }
        format.json { render :show, status: :created, location: @breakpoint_category }
      else
        format.html { render :new }
        format.json { render json: @breakpoint_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /breakpoint_categories/1
  # PATCH/PUT /breakpoint_categories/1.json
  def update
    respond_to do |format|
      if @breakpoint_category.update(breakpoint_category_params)
        format.html { redirect_to "/", notice: 'Breakpoint category was successfully updated.' }
        format.json { render :show, status: :ok, location: @breakpoint_category }
      else
        format.html { render :edit }
        format.json { render json: @breakpoint_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /breakpoint_categories/1
  # DELETE /breakpoint_categories/1.json
  def destroy

    sql = "SET FOREIGN_KEY_CHECKS = 0;"
    records_array = ActiveRecord::Base.connection.execute(sql)

    @breakpoint_category.destroy
    respond_to do |format|
      format.html { redirect_to "/", notice: 'Breakpoint category was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_breakpoint_category
      @breakpoint_category = BreakpointCategory.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def breakpoint_category_params
      params.require(:breakpoint_category).permit(:name, :show_on_home)
    end
end
