class UsersController < ApplicationController

  def index
    redirect_to "/"
  end

  def update_user

      p params

  end

  def guest
    @user = current_user
    respond_to do |format|
      format.html
    end

  end


  def show

    @user = User.find(params[:id])

    if @user.user_type == "C"

      redirect_to guest_user_path(@user)

    else

    if current_user.ads.count > 0
      @ad = current_user.ads.last.id
    end

    if current_user.contacts.count > 0
      @contact = current_user.contacts.first
    else
      @contact = Contact.new
    end

    @breakpoint_match = BreakpointMatch.new

    if current_user.user_type == "A"
        redirect_to "/"
    end



    respond_to do |format|
      format.html
    end

  end

  end

  def admin
    @custom_queries = CustomQuery.all
  end


  def properties
    @user = current_user

    if !@user.username
      flash[:notice] = 'Inserisci prima le tue informazioni, quali nome e numero di telefono, grazie!'
      redirect_to @user
    end

    @props = Property.where({
        :user_id => current_user.id
      })


    respond_to do |format|
      format.html
    end
  end

  def profile
    @user = current_user
    @ad = current_user.ads.last.id
    @breakpoint_match = BreakpointMatch.new

    if current_user.properties.count == 0
      redirect_to properties_users_path
    end



    respond_to do |format|
      format.html
      format.json { render :json => current_user.ads.last.breakpoint_matches.to_json() }
    end

  end



  # PATCH/PUT /breakpoints/1
  # PATCH/PUT /breakpoints/1.json
  def update
    begin
      u = params[:id] == current_user.id ? current_user : User.find(params[:id])
    rescue
      u = current_user
    end
    respond_to do |format|
      if u.update(user_params)
        format.html { redirect_to "/", notice: '' }
        format.json { render :json => u.to_json() }
      else
        format.html { render :edit }
        format.json { render json: u.errors, status: :unprocessable_entity }
      end
    end
  end



  def infos
    @user = current_user
    respond_to do |format|
      format.html
    end
  end


  private

      def user_params
        params.require(:user).permit(:user_type, :username, :tel, :deal_from, :deal_to, :payment_method, :deal_notes)
      end

end
