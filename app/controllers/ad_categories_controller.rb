class AdCategoriesController < ApplicationController
  before_action :set_ad_category, only: [:show, :edit, :update, :destroy]

  # GET /ad_categories
  # GET /ad_categories.json
  def index

    if !current_user
    else
        if !current_user.username
          redirect_to current_user
        end
    end
    @ad_categories = AdCategory.all

    @q = Ad.ransack(params[:q])

    @ads = @q.result(distinct: true).page(params[:page]).per(3)

    p params
    @title = Default.find_by_field("home_generico").value.html_safe;

    # AdCategory.find_each(&:save)
  end

  # GET /ad_categories/1
  # GET /ad_categories/1.json
  def show
  end

  # GET /ad_categories/1
  # GET /ad_categories/1.json
  def disclaimer
  end

  # GET /ad_categories/1
  # GET /ad_categories/1.json
  def infos
    @txt = Default.find_by_field(params[:p]).value.html_safe;

  end

  # GET /ad_categories/new
  def new
    @ad_category = AdCategory.new
  end

  # GET /ad_categories/1/edit
  def edit
  end

  # POST /ad_categories
  # POST /ad_categories.json
  def create
    @ad_category = AdCategory.new(ad_category_params)

    respond_to do |format|
      if @ad_category.save
        format.html { redirect_to @ad_category, notice: 'Ad category was successfully created.' }
        format.json { render :show, status: :created, location: @ad_category }
      else
        format.html { render :new }
        format.json { render json: @ad_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /ad_categories/1
  # PATCH/PUT /ad_categories/1.json
  def update
    respond_to do |format|
      if @ad_category.update(ad_category_params)
        format.html { redirect_to @ad_category, notice: 'Ad category was successfully updated.' }
        format.json { render :show, status: :ok, location: @ad_category }
      else
        format.html { render :edit }
        format.json { render json: @ad_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ad_categories/1
  # DELETE /ad_categories/1.json
  def destroy
    @ad_category.destroy
    respond_to do |format|
      format.html { redirect_to ad_categories_url, notice: 'Ad category was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ad_category
      @ad_category = AdCategory.friendly.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ad_category_params
      params.require(:ad_category).permit(:name, :slug, :tooltip)
    end
end
