require 'paperclip_processors/watermark'
class Picture < ApplicationRecord
  belongs_to :ad
  default_scope { order(order_nr: :asc) }

  has_attached_file :attachment,
                      styles: {
                        :crop => "500x500#",
                        # original: {
                        #   geometry:       "800>",
                        #   auto_orient:    false,
                        #   position: "Center"
                        # },
                        :vertical => "640x1136#"
                      }

  validates_attachment_content_type :attachment, content_type: /\Aimage\/.*\Z/


  def cropped
    self.attachment.url(:crop)
  end


end
