class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :breakpoint_matches
  has_many :properties
  has_many :comments
  has_many :contacts
  has_many :ads
  has_many :favs

  PAYMENT_METHODS = ["Contanti", "Assegno", "Bonifico", "Paypal"]

end
