class Ad < ApplicationRecord
  belongs_to :user
  belongs_to :ad_category
  has_many :breakpoint_matches
  has_many :pictures
  has_many :ubiquities
  has_many :comments

  geocoded_by :address   # can also be an IP address
  after_validation :geocode          # auto-fetch coordinates
  after_create :set_title
  accepts_nested_attributes_for :pictures


  extend FriendlyId
  friendly_id :title, use: :slugged

  def amount
    Ad.where({
        :base => self.base,
        :ad_category_id => self.ad_category_id
      }).count
  end


  def set_title
    self.update_attributes({
        :title => self.user.username
      })
  end

  def google_map(center)
    "https://maps.googleapis.com/maps/api/staticmap?center=#{center}&size=800x400&zoom=17"
  end

end
