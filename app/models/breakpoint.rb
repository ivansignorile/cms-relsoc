class Breakpoint < ApplicationRecord
  belongs_to :breakpoint_category
  default_scope { order(:name) } 
end
