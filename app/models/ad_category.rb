class AdCategory < ApplicationRecord
  extend FriendlyId
  has_many :ads
  friendly_id :name, use: :slugged
end
