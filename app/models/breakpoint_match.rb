class BreakpointMatch < ApplicationRecord
  belongs_to :breakpoint
  belongs_to :ad
  belongs_to :breakpoint_category
end
