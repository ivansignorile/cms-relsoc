class City < ApplicationRecord
  default_scope { order('name ASC') }

  def full_name
     name+" ("+prov+")"
  end
end
