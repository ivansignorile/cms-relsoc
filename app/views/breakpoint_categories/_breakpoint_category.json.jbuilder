json.extract! breakpoint_category, :id, :name, :created_at, :updated_at
json.url breakpoint_category_url(breakpoint_category, format: :json)
