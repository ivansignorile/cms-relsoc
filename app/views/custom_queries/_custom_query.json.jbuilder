json.extract! custom_query, :id, :title, :path, :created_at, :updated_at
json.url custom_query_url(custom_query, format: :json)
