json.extract! breakpoint_match, :id, :breakpoint_id, :ad_id, :breakpoint_category_id, :created_at, :updated_at
json.url breakpoint_match_url(breakpoint_match, format: :json)
