json.extract! issue, :id, :name, :email, :city, :motivation, :content, :created_at, :updated_at
json.url issue_url(issue, format: :json)
