json.extract! ad, :id, :user_id, :ad_category_id, :address, :latitude, :longitude, :description, :views_count, :queries, :created_at, :updated_at
json.url ad_url(ad, format: :json)
