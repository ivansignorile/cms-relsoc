json.extract! ad_category, :id, :name, :created_at, :updated_at
json.url ad_category_url(ad_category, format: :json)
