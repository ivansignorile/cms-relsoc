json.extract! breakpoint, :id, :breakpoint_category_id, :name, :created_at, :updated_at
json.url breakpoint_url(breakpoint, format: :json)
