json.extract! contact, :id, :user_id, :whatsapp, :mobile, :email, :skype, :facebook, :twitter, :instagram, :snapchat, :website, :created_at, :updated_at
json.url contact_url(contact, format: :json)
