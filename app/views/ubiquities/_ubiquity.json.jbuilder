json.extract! ubiquity, :id, :ad_id, :from, :to, :created_at, :updated_at
json.url ubiquity_url(ubiquity, format: :json)
