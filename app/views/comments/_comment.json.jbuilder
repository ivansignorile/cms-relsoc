json.extract! comment, :id, :user_id, :ad_id, :content, :title, :created_at, :updated_at
json.url comment_url(comment, format: :json)
