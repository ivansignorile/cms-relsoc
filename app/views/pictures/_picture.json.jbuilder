json.extract! picture, :id, :ad_id, :created_at, :updated_at
json.url picture_url(picture, format: :json)
