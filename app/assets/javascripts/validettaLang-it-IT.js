(function($){
	$.fn.validettaLanguage = {};
	$.validettaLanguage = {
		init : function(){
			$.validettaLanguage.messages = {
				required	: 'Il campo è obbligatorio.', 
				email		: 'E-mail non valida.', 
				number		: 'Questo campo accetta solo valori numerici.', 
				maxLength	: 'Questo campo accetta un massimo di {count} caratteri.', 
				minLength	: 'Questo campo richiede almeno {count} caratteri.', 
				maxChecked	: 'Si possono selezionare al massimo {count} opzioni.', 
				minChecked	: 'Bisogna selezionare almeno {count} opzioni.', 
				maxSelected	: 'Si possono selezionare al massimo {count} opzioni.', 
				minSelected	: 'Bisogna selezionare almeno {count} opzioni.', 
				notEqual	: 'I campi non coincidono', 
				creditCard	: 'Carta di credito non valida.'
			};
		}
	};
	$.validettaLanguage.init();

	if(jQuery.fn.pickadate){
	jQuery.extend( jQuery.fn.pickadate.defaults, {
	    monthsFull: [ 'gennaio', 'febbraio', 'marzo', 'aprile', 'maggio', 'giugno', 'luglio', 'agosto', 'settembre', 'ottobre', 'novembre', 'dicembre' ],
	    monthsShort: [ 'gen', 'feb', 'mar', 'apr', 'mag', 'giu', 'lug', 'ago', 'set', 'ott', 'nov', 'dic' ],
	    weekdaysFull: [ 'domenica', 'lunedì', 'martedì', 'mercoledì', 'giovedì', 'venerdì', 'sabato' ],
	    weekdaysShort: [ 'dom', 'lun', 'mar', 'mer', 'gio', 'ven', 'sab' ],
	    today: 'Oggi',
	    clear: 'Cancellare',
	    close: 'Chiudi',
	    firstDay: 1,
	    format: 'dddd d mmmm yyyy',
	    formatSubmit: 'yyyy/mm/dd'
	});
	}
})(jQuery);