var Utils = function() {};
String.prototype
Utils.prototype = {
    unblockUI: function(){
        $(document).ajaxStop($.unblockUI);
    },
    blockUI: function(){
        $.blockUI({
            css: {
                backgroundColor: 'transparent',
                color: '#111',
                border: "none"
            },
            message: "<img style='width:250px;' src='/assets/logo.png' /><br /><br /> Caricamento in corso...",
            overlayCSS: {
                opacity: 0.8,
                backgroundColor: "#fff"
            }
        })
    },
    imageHeightFix: function(sel, callback){
      function onImageReady(selector, handler) {
          var list;

          // If given a string, use it as a selector; else use what we're given
          list = typeof selector === 'string' ? $(selector) : selector;

          // Hook up each image individually
          list.each(function(index, element) {
            if (element.complete) {
                // Already loaded, fire the handler (asynchronously)
                setTimeout(function() {
                    fireHandler.call(element);
                }, 0); // Won't really be 0, but close
            }
            else {
                // Hook up the handler
                $(element).bind('load', fireHandler);
            }
          });

          function fireHandler(event) {
            // Unbind us if we were bound
            $(this).unbind('load', fireHandler);

            // Call the handler
            handler.call(this);
          }
      }
      onImageReady(sel, callback);

      },
    geoaddress : function(id,city) {
    var placeSearch, autocomplete;
    var componentForm = {
      street_number: 'short_name',
      route: 'long_name',
      locality: 'long_name',
      administrative_area_level_1: 'short_name',
      country: 'long_name',
      postal_code: 'short_name'
    };

    function initAutocomplete(city) {
      if(!city){
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById(id)),
            {types: ['geocode']});
      }else{
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById(id)),
            {
              types: ['geocode'],
              componentRestrictions: {city: city}
            }
        );

      }

      autocomplete.addListener('place_changed', fillInAddress);
    }

    function fillInAddress() {
      var place = autocomplete.getPlace();

      for (var component in componentForm) {
        document.getElementById(component).value = '';
        document.getElementById(component).disabled = false;
      }

      for (var i = 0; i < place.address_components.length; i++) {
        var addressType = place.address_components[i].types[0];
        if (componentForm[addressType]) {
          var val = place.address_components[i][componentForm[addressType]];
          document.getElementById(addressType).value = val;
        }
      }
    }

      function geolocate() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
          });
        }
      }
      $("#"+id).on("focus",function(){
        geolocate();
      })
      initAutocomplete(city)
    },
    trimToLength: function(text, size) {
        return (text.length > size) ? jQuery.trim(text).substring(0, size).split(" ").slice(0, -1).join(" ") + "..." : text;
    },
    autocomplete: function(element) {
      $(element).typeahead()
    },
    return_error: function(error) {
        // console.log(error.responseText);
    },
    return_success: function(message) {
        // console.log(message);
    },
    return_param: function(param_name) {
        var sPageURL = window.location.search.substring(1);
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++) {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == param_name) {
                return sParameterName[1];
            }
        }
    },
    calendar: function(params){
          var BAD_ATTR_RE = /\]_submit$/;
          var now = moment();
            params.field.pickadate({
              format: 'dd-mm-yyyy',
              formatSubmit : "dd-mm-yyyy",
              hiddenSuffix:'',
              selectYears: true,
              selectMonths: true,
              selectYears: 100,
            }).on("change", function(){
                params.callback();
            })
    },
    getSuggestions: function(params){
       var bh = new Bloodhound({
         datumTokenizer: Bloodhound.tokenizers.obj.whitespace(params.key),
         queryTokenizer: Bloodhound.tokenizers.whitespace,
         remote: params.url
       });

       bh.initialize();

       params.field.typeahead(null, {
         name: params.title,
         displayKey: params.key,
         source: bh.ttAdapter()
       });


       params.field.typeahead(null, {
         name: 'countries',
         displayKey: 'name',
         source: countries.ttAdapter()
       });


    },
    put: function(params) {
        var that = this;
        console.log(params)
        $.when($.ajax({
            type: "PUT",
            url: params.url,
            data: JSON.stringify(params.data),
            contentType: "application/json",
            dataType: "json"
        })).done(function(response) {
            params.callback(response);
        }).fail(function(response) {
            params.onError ? params.onError(response) : that.return_error(response);
        });
    },
    post: function(params) {
        var that = this;
        $.when($.ajax({
            type: "POST",
            url: params.url,
            data: JSON.stringify(params.data),
            contentType: "application/json",
            dataType: "json"
        })).done(function(response) {
            params.callback(response);
        }).fail(function(response) {
            that.return_error(response);
        });
    },
    delete: function(params) {
        var that = this;
        $.when($.ajax({
            type: "DELETE",
            url: params.url,
            contentType: "application/json",
            dataType: "json"
        })).done(function(response) {
            params.callback(response);
        }).fail(function(response) {
            that.return_error(response);
        });
    },
    get: function(params) {
      //  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');

        var that = this;
        $.when($.ajax({
            type: "GET",
            url: params.url,
            contentType: "application/json",
            dataType: "json"
        })).done(function(response) {
            params.callback(response);
        }).fail(function(response) {
            that.return_error(response);
        });
    },
    noti: function(params){
      var _n = new Noty({
          text: params.message,
          layout   : 'topRight',
          type    : 'info',
          closeWith: ['click', 'button']
      })
      _n.show();
      setTimeout(function(){
          _n.close()
      },1500)
    },
    params: function(key) {
        var sPageURL = window.location.search.substring(1);
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++) {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == key) {
                return sParameterName[1];
            }
        }
    }
}
utils = new Utils();
