var timesheet = (function(){

  function setHourRecap(typology){
          var _elements = $('*[data-col="'+typology+'"]');
          var _tot = 0;
          $.each(_elements,function(i,e){
              _tot = _tot + parseFloat($(e).val())
          })
          $("[data-totalcol='"+typology+"']").html(parseFloat(_tot).toFixed(2))
  }

  function setDayRecap(){
    var _elements = $('*[data-col="0"]');
    var days = 0;
    $.each(_elements,function(i,d){
        if($(d).val() != 0){
          days ++;
        }
    })
    $("#workedDays").html(parseInt(days))
  }


  return {
      setTotal : function(response){
          $("#tot_"+response.id).val(parseFloat(response.total).toFixed(2));
          setHourRecap(response.typology);
          setDayRecap()
      },
      init : function(){

        setHourRecap(0);
        setHourRecap(1);
        setHourRecap(2);
        setDayRecap();

        $("#date_search").on("click",function(){
            window.location.href = "/users/timesheet?m="+$("#date_month").val()+"&y="+$("#date_year").val()
        })

      }
  }
})()

$(document).ready(function(){
    timesheet.init();
})
