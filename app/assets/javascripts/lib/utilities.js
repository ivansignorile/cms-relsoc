var utilities = (function(){
      return {
        id: function(){
          return window.location.href.match(/\d+$/);
        },
        params: function(param_name) {
            var sPageURL = window.location.search.substring(1);
            var sURLVariables = sPageURL.split('&');
            for (var i = 0; i < sURLVariables.length; i++) {
                var sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] == param_name) {
                    return sParameterName[1];
                }
            }
        },
        notice: function(txt){
            utils.noti({
                message: txt
            })
        },
        log: function(txt){
          console.log(txt)
        },
        get: function(params){
          var that = this;
          $.when($.ajax({
              type: "GET",
              url: params.url,
              contentType: "application/json",
              dataType: "json"
          })).done(function(response) {
              params.callback(response);
          }).fail(function(response) {
              utilities.log(response);
          });
        },
        post: function(params) {
            var that = this;
            console.log(params.data)
            $.when($.ajax({
                type: "POST",
                url: params.url,
                data: JSON.stringify(params.data),
                contentType: "application/json",
                dataType: "json"
            })).done(function(response) {
                params.callback(response);
            }).fail(function(response) {
              utilities.log(response);
            });
        },
        update: function(e, callback){
            var _data = {};
            _data[$(e).data("attr")] = $(e).val();
            utilities.notice("Salvataggio in corso...");
            $.when($.ajax({
                type: "PUT",
                url: "/"+$(e).data("controller")+"/"+$(e).data("id"),
                data: JSON.stringify(_data),
                contentType: "application/json",
                dataType: "json"
            })).done(function(response) {
                if(callback){callback(response)}
                utilities.notice("Modifiche salvate");
                setTimeout(function(){
                  utilities.notice("");
                },2500)
            }).fail(function(response) {
                utilities.notice("Si è verificato un errore.");
                // params.onError ? params.onError(response) : utilities.log(response);
            });
        }
      }
})()
