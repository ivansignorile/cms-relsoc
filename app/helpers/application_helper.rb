module ApplicationHelper
  def createUser(email,password,username,tel,full_name)
      user = User.find_by_email(email)
      if !user
        user = User.new({
            :email => email,
            :password => password,
            :username => full_name.downcase,
            :tel => tel
          })
         user.save!
      end
      user
  end

  def savePic(url,ad_id)
    picture = Picture.new({
        :attachment => URI.parse(url),
        :ad_id => ad_id
      })

    picture.save!
    picture
  end

  def saveBreakpointMatch(bp,ad_id)
      bm = BreakpointMatch.new({
          :breakpoint_category_id => bp.breakpoint_category_id,
          :breakpoint_id => bp.id,
          :ad_id => ad_id
        })
      bm.save!
      bm
  end

  def saveBreakpoint(breakpoint_value, breakpoint_category_value, ad_id)
      bc = BreakpointCategory.find(breakpoint_category_value)
      bps = Breakpoint.where({
            :breakpoint_category_id => bc.id,
            :name => bc.name
      })
      if bps.count > 0
        bp = bps.first
      else
        bp = Breakpoint.new({
            :breakpoint_category_id => bc.id,
            :name => breakpoint_value
          })
        bp.save!
      end

      saveBreakpointMatch(bp,ad_id)

  end


  def saveProperties(property_value, property_category_value, user_id)
      pc = PropertyCategory.find(property_category_value)

      pps = Property.where({
          :property_category_id => pc.id,
          :user_id => user_id,
          :value => property_value
        })

      if pps.count > 0

      else
        pp = Property.new({
            :property_category_id => pc.id,
            :user_id => user_id,
            :value => property_value
          })
        pp.save!
      end

  end

  def createAd(user_id, ad_category_id, address,description,_qs,zona)
    is_first_ad = false
    ads = Ad.where({
        :description => description,
        :user_id => user_id
      })
    if ads.count > 0
      ad = ads.first
    else
    ad = Ad.new({
        :user_id => user_id,
        :ad_category_id => ad_category_id,
        :base => address,
        :zone => zona || address,
        :address => address,
        :description => description,
        :queries => _qs.to_s
      })
    ad.save!
    is_first_ad = true
    # sleep(5)
    end
    [ad, is_first_ad]
  end


end
