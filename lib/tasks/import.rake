#encoding: utf-8
require 'rubygems'
require 'open-uri'
require 'paperclip'
require 'roo'



namespace :import do




  task :fix, [:id] => :environment do |t, args|

    User.all.each do |u|
        username = u.username.sub!("incontriitalia", "")
        p username
        u.update_attributes({
            :username => username
          })
    end

  end



  task :escortadvisor, [:id] => :environment do |t, args|

    xlsx = Roo::Spreadsheet.open('lib/tasks/escortadvisor.xlsx')

    xlsx.each_with_index do |row, index|
      begin
        user = User.find_by_username(row[3])
        if !user
        user = User.new({
            :email => row[3].downcase.gsub(/\s+/, "")+"@rs.com",
            :password => "escortadvisor",
            :username => row[3],
            :tel => row[5]
          })

        user.save!
        end

        if row[6] && row[6] != ""
          # breakpoint_category = BreakpointCategory.find_by_name("Nazionalità")
          # breakpoint = Breakpoint.find_by_name(row[5])
          #
          # if !breakpoint
          #   breakpoint = Breakpoint.new({
          #         :name => row[5]
          #     })
          #   breakpoint.save!
          # end

          if !user
          ad = Ad.new({
              :user_id => user.id,
              :ad_category_id => 2,
              :address => row[6],
              :description => row[8]
            })

          ad.save!
          else
            ads = Ad.where({:user_id => user.id})
            if ads.count == 0
              ad = Ad.new({
                  :user_id => user.id,
                  :ad_category_id => 2,
                  :address => row[6],
                  :description => row[8]
                })

              ad.save!
            else
            ad = ads.last
            end
          end

          begin

          # picture = Picture.new({
          #     :attachment => URI.parse(row[6]),
          #     :ad_id => ad.id
          #   })
          #
          # picture.save!

        rescue
        end


          # breakpoint_match = BreakpointMatch.new({
          #       :user_id => user.id,
          #       :breakpoint_id => breakpoint.id,
          #       :breakpoint_category_id => breakpoint_category.id,
          #       :ad_id => ad.id
          #   })
          #
          # breakpoint_match.save!


        end

      rescue => err
        p err
      end



    end





    end








    task :escortforum, [:id] => :environment do |t, args|

      xlsx = Roo::Spreadsheet.open('lib/tasks/escortforum.xlsx')

      xlsx.each_with_index do |row, index|
        begin
          user = User.find_by_username(row[3])
          if !user
          user = User.new({
              :email => row[3].downcase.gsub(/\s+/, "")+"@rs.com",
              :password => "escortforum",
              :username => row[3],
              :tel => row[4]
            })

          user.save!
          end

          if row[6] && row[6] != ""
            # breakpoint_category = BreakpointCategory.find_by_name("Nazionalità")
            # breakpoint = Breakpoint.find_by_name(row[5])
            #
            # if !breakpoint
            #   breakpoint = Breakpoint.new({
            #         :name => row[5]
            #     })
            #   breakpoint.save!
            # end

            if !user
            ad = Ad.new({
                :user_id => user.id,
                :ad_category_id => 1,
                :address => row[6],
                :description => row[7]
              })

            ad.save!
            else
              ads = Ad.where({:user_id => user.id})
              if ads.count == 0
                ad = Ad.new({
                    :user_id => user.id,
                    :ad_category_id => 1,
                    :address => row[6],
                    :description => row[7]
                  })

                ad.save!
              else
              ad = ads.last
              end
            end

            begin

            # picture = Picture.new({
            #     :attachment => URI.parse(row[6]),
            #     :ad_id => ad.id
            #   })
            #
            # picture.save!

          rescue
          end


            # breakpoint_match = BreakpointMatch.new({
            #       :user_id => user.id,
            #       :breakpoint_id => breakpoint.id,
            #       :breakpoint_category_id => breakpoint_category.id,
            #       :ad_id => ad.id
            #   })
            #
            # breakpoint_match.save!


          end

        rescue => err
          p err
        end



      end





      end









      task :incontriitalia, [:id] => :environment do |t, args|

        xlsx = Roo::Spreadsheet.open('lib/tasks/incontriitalia.xlsx')

        xlsx.each_with_index do |row, index|
          p row[11]
          begin
            user = User.find_by_username(row[0])
            if !user
            user = User.new({
                :email => (row[0]+"incontriitalia").downcase.gsub(/\s+/, "")+"@rs.com",
                :password => "incontriitalia",
                :username => row[0],
                :tel => row[8]
              })

            user.save!
            end

            if row[6] && row[6] != ""
              breakpoint_category = BreakpointCategory.find_by_name("Nazionalità")
              breakpoint = Breakpoint.find_by_name(row[5])

              if !breakpoint
                breakpoint = Breakpoint.new({
                      :name => row[5]
                  })
                breakpoint.save!
              end

              if !user
              ad = Ad.new({
                  :user_id => user.id,
                  :ad_category_id => 1,
                  :address => row[9],
                  :description => row[7]
                })

              ad.save!
              else
                ads = Ad.where({
                  :user_id => user.id,
                  :description => row[7]
                })
                if ads.count == 0
                  ad = Ad.new({
                      :user_id => user.id,
                      :ad_category_id => 1,
                      :address => row[9],
                      :description => row[7]
                    })

                  ad.save!
                else
                ad = ads.last
                end
              end

              begin

              picture = Picture.new({
                  :attachment => URI.parse(row[11]),
                  :ad_id => ad.id
                })

              picture.save!

            rescue
            end


              # breakpoint_match = BreakpointMatch.new({
              #       :user_id => user.id,
              #       :breakpoint_id => breakpoint.id,
              #       :breakpoint_category_id => breakpoint_category.id,
              #       :ad_id => ad.id
              #   })
              #
              # breakpoint_match.save!


            end

          rescue => err
            p err
          end

          sleep 30


        end





        end








  task :all, [:id] => :environment do |t, args|

    xlsx = Roo::Spreadsheet.open('lib/tasks/rosarossa.xlsx')

    xlsx.each_with_index do |row, index|
        user = User.find_by_username(row[0])
        if !user
        user = User.new({
            :email => row[0].downcase.gsub(/\s+/, "")+"@rs.com",
            :password => "password",
            :username => row[0],
            :tel => row[3]
          })

        user.save!
        end

        if row[6] && row[6] != ""
          # breakpoint_category = BreakpointCategory.find_by_name("Nazionalità")
          # breakpoint = Breakpoint.find_by_name(row[5])
          #
          # if !breakpoint
          #   breakpoint = Breakpoint.new({
          #         :name => row[5]
          #     })
          #   breakpoint.save!
          # end

          if !user
          ad = Ad.new({
              :user_id => user.id,
              :ad_category_id => 1,
              :address => row[2],
              :description => row[1]
            })

          ad.save!
          else
            ads = Ad.where({:user_id => user.id})
            if ads.count == 0
              ad = Ad.new({
                  :user_id => user.id,
                  :ad_category_id => 1,
                  :address => row[2],
                  :description => row[1]
                })

              ad.save!
            else
            ad = ads.last
            end
          end

          begin

          picture = Picture.new({
              :attachment => URI.parse(row[6]),
              :ad_id => ad.id
            })

          picture.save!

        rescue
        end


          # breakpoint_match = BreakpointMatch.new({
          #       :user_id => user.id,
          #       :breakpoint_id => breakpoint.id,
          #       :breakpoint_category_id => breakpoint_category.id,
          #       :ad_id => ad.id
          #   })
          #
          # breakpoint_match.save!


        end





    end





    end

end
