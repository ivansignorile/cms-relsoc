#encoding: utf-8
require "#{Rails.root}/app/helpers/application_helper"
include ApplicationHelper
require 'csv'

require 'rubygems'
require 'open-uri'
require 'paperclip'
require 'roo'



namespace :imp do



  task :lang, [:id] => :environment do |t, args|
    sql = "SET FOREIGN_KEY_CHECKS = 0;"
    records_array = ActiveRecord::Base.connection.execute(sql)

      old_breakpoints = Breakpoint.where({
        :breakpoint_category_id => 5
        })

      old_breakpoints.each do |old|
          old.destroy
      end

      _l = ["Arabo", "Bulgaro", "Cinese", "Ceco", "Croato", "Finlandese", "Francese", "Giapponese", "Greco", "Inglese", "Italiano", "Lituano", "Polacco", "Portoghese", "Rumeno", "Russo", "Slovacco", "Sloveno", "Spagnolo", "Tedesco", "Turco", "Ungherese" ]

      _l.each do |l|

        _b = Breakpoint.new({
            :breakpoint_category_id => 5,
            :name => l
          })

        _b.save!

      end


  end




  task :abstract, [:id] => :environment do |t, args|

      Ad.all.each do |ad|
        ad.update_attributes({
            :abstract => ad.title.capitalize+" "+ad.ad_category.name.downcase+" di base a "+ad.base
          })
      end

  end

  task :fix, [:id] => :environment do |t, args|
      Ad.where({
          :address => ""
        }).each do |ad|
        ad.update_attributes({
            :address => ad.base
          })
      end

  end

  task :noanonimi, [:id] => :environment do |t, args|
      Ad.where({
          :zone => "No anonimi"
        }).each do |ad|
        ad.update_attributes({
            :zone => nil
          })
      end

  end

  task :props, [:id] => :environment do |t, args|
    sql = "SET FOREIGN_KEY_CHECKS = 0;"
    records_array = ActiveRecord::Base.connection.execute(sql)

    User.all.each do |uu|
        if uu.properties.count > 3
            uu.properties.each_with_index do |pp, ind|
                if ind > 3
                  p "caso"
                  pp.destroy
                end
            end
        end
    end

  end



  task :clean, [:id] => :environment do |t, args|
    sql = "SET FOREIGN_KEY_CHECKS = 0;"
    records_array = ActiveRecord::Base.connection.execute(sql)

    Ad.all.each do |ad|
      if !ad.user.tel
        ad.destroy
      end

      if !ad.address
        ad.destroy
      end

      if ad.pictures.count == 0
        ad.destroy
      end

    end

  end


  task :properties, [:id] => :environment do |t, args|
    xlsx = Roo::Spreadsheet.open('lib/tasks/properties.xlsx')

    xlsx.each_with_pagename do |name, sheet|
      placeholder = ""
      sheet.each do |row|
          placeholder = placeholder+","+row[0].to_s
      end
      pc = PropertyCategory.new({:name => name, :placeholder => placeholder.sub!(",","")})
      pc.save!
    end

  end


  task :categories, [:id] => :environment do |t, args|
    xlsx = Roo::Spreadsheet.open('lib/tasks/categories.xlsx')

    xlsx.each_with_pagename do |name, sheet|
      bc = BreakpointCategory.new({:name => name})
      bc.save!
      sheet.each do |row|
          bp = Breakpoint.new({
              :name => row[0],
              :breakpoint_category_id => bc.id
            })
            bp.save!
      end
    end

  end



  task :cities, [:id] => :environment do |t, args|
    xlsx = Roo::Spreadsheet.open('lib/tasks/comuni.xlsx')

    xlsx.each_with_index do |row, index|
       city = City.new({
          :name => row[4],
          :prov => row[3]
         })
        city.save!
    end

  end


  task :base, [:id] => :environment do |t, args|

      Ad.all.each do |ad|
        ad.update_attributes({
            :base => ad.address
          })
      end
  end
  task :intour, [:id] => :environment do |t, args|
      ads = Ad.where('ads.description LIKE "%tour%"')
      ads.each do |ad|
        ad.update_attributes({
            :in_tour => true
          })
      end
  end

  task :new_pics, [:id] => :environment do |t, args|

      (1..60).each do |integer|
        offset = rand(Ad.count)
        ad = Ad.offset(offset).first
        ad.update_attributes({
            :new_photo => true
          })

      end



  end



  # incontriitalia
  task :incontriitalia, [:id] => :environment do |t, args|
    xlsx = Roo::Spreadsheet.open('lib/tasks/incontriitalia.xlsx')



    xlsx.each_with_index do |row, index|

        if index > 0
           begin
           username = row[0].downcase.gsub(/\s+/, "")
           #  email,password,username,tel
           user = createUser(username+"@rs.com","incontriitalia_#{index.to_s}",username,row[8].sub!("Telefono ", ""),row[0])
           rescue
           end
        end

        if user
          begin
            # user_id, ad_category_id, address,description,queries
            _qs = [
                row[1],
                row[2],
                row[3],
                row[4],
                row[5],
                row[6]
              ];
            ad = createAd(user.id,1,row[9],row[7],_qs)

            # salvo i breakpoints se ci sono
            if ad[1]
                saveProperties(row[2], 6, user.id)
                saveProperties(row[3], 2, user.id)
                saveProperties(row[4], 1, user.id)
                saveBreakpoint(row[5], 3, ad[0].id)
                saveBreakpoint(row[6], 5, ad[0].id)
            end

            savePic(row[11], ad[0].id)


          rescue => error
            p error
          end
        end

        # di advisor abbiamo solo l'età come breakpoint_category

    end

  end




    # escortmantra
    task :escortmantra, [:id] => :environment do |t, args|
      xlsx = Roo::Spreadsheet.open('lib/tasks/escortmantraOK.xlsx')


      xlsx.each_with_index do |row, index|



          if index > 0
             begin
             username = row[0].downcase.gsub(/\s+/, "")
             #  email,password,username,tel
             user = createUser(username+"@rs.com","escortmantra_#{index.to_s}",username,row[1],row[0])
             rescue
             end


          end

          if user
            begin


              p "____"


              # user_id, ad_category_id, address,description,queries
              _qs = [
                  row[5],
                  row[6],
                  row[7],
                  row[8],
                  row[9],
                  row[10]
                ];

              # row[15] zona
              if user.tel
              ad = createAd(user.id,1,row[2],row[4],_qs,row[15] )

              # salvo i breakpoints se ci sono
              # if ad[1]
              #     saveProperties(row[2], 6, user.id)
              #     saveProperties(row[3], 2, user.id)
              #     saveProperties(row[4], 1, user.id)
              #     saveBreakpoint(row[5], 3, ad[0].id)
              #     saveBreakpoint(row[6], 5, ad[0].id)
              # end
              #

              savePic(row[19], ad[0].id)

              end

            rescue => error
              # p error
            end
          end

          # di advisor abbiamo solo l'età come breakpoint_category

      end

    end







      # incontriitalia
      task :rosarossa_escort, [:id] => :environment do |t, args|
        xlsx = Roo::Spreadsheet.open('lib/tasks/rosarossa_escort.xlsx')



        xlsx.each_with_index do |row, index|

            if index > 0
               begin
               username = row[0].downcase.gsub(/\s+/, "")
               #  email,password,username,tel
               user = createUser(username+"@rs.com","rosarossa_escort#{index.to_s}",username,row[1].gsub!(". ", ""),row[0])
               rescue
               end
            end

            if user && row[4]
              begin
                # user_id, ad_category_id, address,description,queries
                _qs = [
                    row[3]
                  ];
                aa = row[4].split(" ")
                ad = createAd(user.id,1,aa[aa.count - 1],row[5],_qs, false)

                # salvo i breakpoints se ci sono
                # if ad[1]
                #     saveProperties(row[2], 6, user.id)
                #     saveProperties(row[3], 2, user.id)
                #     saveProperties(row[4], 1, user.id)
                #     saveBreakpoint(row[5], 3, ad[0].id)
                #     saveBreakpoint(row[6], 5, ad[0].id)
                # end
                p row[6]
                savePic(row[6], ad[0].id)


              rescue => error
                p error
              end
            end

            # di advisor abbiamo solo l'età come breakpoint_category

        end

      end




      # incontriitalia
      task :rosarossa_massaggiatrici, [:id] => :environment do |t, args|
        xlsx = Roo::Spreadsheet.open('lib/tasks/rosarossa_massaggiatrici.xlsx')



        xlsx.each_with_index do |row, index|

            if index > 0
               begin
               username = row[0].downcase.gsub(/\s+/, "")
               #  email,password,username,tel
               user = createUser(username+"@rs.com","rosarossa_massaggiatrici#{index.to_s}",username,row[1].gsub!(".", ""),row[0])
               rescue
               end
            end

            if user && row[2]
              begin
                # user_id, ad_category_id, address,description,queries
                _qs = [
                    row[3]
                  ];
                aa = row[2]
                ad = createAd(user.id,3,aa,row[3],_qs, false)

                # salvo i breakpoints se ci sono
                # if ad[1]
                #     saveProperties(row[2], 6, user.id)
                #     saveProperties(row[3], 2, user.id)
                #     saveProperties(row[4], 1, user.id)
                #     saveBreakpoint(row[5], 3, ad[0].id)
                #     saveBreakpoint(row[6], 5, ad[0].id)
                # end
                 savePic(row[4], ad[0].id)


              rescue => error
                p error
              end
            end

            # di advisor abbiamo solo l'età come breakpoint_category

        end

      end




      # incontriitalia
      task :rosarossa_girls, [:id] => :environment do |t, args|
        xlsx = Roo::Spreadsheet.open('lib/tasks/rosarossa_girls.xlsx')



        xlsx.each_with_index do |row, index|

            if index > 0
               begin
               username = row[0].downcase.gsub(/\s+/, "")
               #  email,password,username,tel
               user = createUser(username+"@rs.com","rosarossa_girls#{index.to_s}",username,row[3].gsub!(".", ""),row[0])
               rescue
               end
            end

            if user && row[2]
              begin
                # user_id, ad_category_id, address,description,queries
                _qs = [
                    row[1]
                  ];
                aa = row[2]
                ad = createAd(user.id,2,aa,row[1],_qs, false)

                # salvo i breakpoints se ci sono
                # if ad[1]
                #     saveProperties(row[2], 6, user.id)
                #     saveProperties(row[3], 2, user.id)
                #     saveProperties(row[4], 1, user.id)
                #     saveBreakpoint(row[5], 3, ad[0].id)
                #     saveBreakpoint(row[6], 5, ad[0].id)
                # end
                 savePic(row[6], ad[0].id)


              rescue => error
                p error
              end
            end

            # di advisor abbiamo solo l'età come breakpoint_category

        end

      end















end
