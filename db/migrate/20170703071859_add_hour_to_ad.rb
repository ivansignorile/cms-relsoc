class AddHourToAd < ActiveRecord::Migration[5.0]
  def change
    add_column :ads, :hour, :text
  end
end
