class AddSlugToAdCategory < ActiveRecord::Migration[5.0]
  def change
    add_column :ad_categories, :slug, :string, :uniq => true
  end
end
