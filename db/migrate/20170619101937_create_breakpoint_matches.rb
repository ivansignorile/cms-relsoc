class CreateBreakpointMatches < ActiveRecord::Migration[5.0]
  def change
    create_table :breakpoint_matches do |t|
      t.belongs_to :breakpoint, foreign_key: true
      t.belongs_to :ad, foreign_key: true
      t.belongs_to :breakpoint_category, foreign_key: true

      t.timestamps
    end
  end
end
