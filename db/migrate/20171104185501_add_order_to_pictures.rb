class AddOrderToPictures < ActiveRecord::Migration[5.0]
  def change
    add_column :pictures, :order_nr, :integer
  end
end
