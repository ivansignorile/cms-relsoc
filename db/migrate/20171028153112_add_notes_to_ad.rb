class AddNotesToAd < ActiveRecord::Migration[5.0]
  def change
    add_column :ads, :notes, :text
  end
end
