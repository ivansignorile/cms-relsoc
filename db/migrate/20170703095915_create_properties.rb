class CreateProperties < ActiveRecord::Migration[5.0]
  def change
    create_table :properties do |t|
      t.belongs_to :property_category, foreign_key: true
      t.belongs_to :user, foreign_key: true
      t.string :value

      t.timestamps
    end
  end
end
