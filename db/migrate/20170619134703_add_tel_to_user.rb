class AddTelToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :tel, :text
  end
end
