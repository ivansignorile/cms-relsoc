class CreateBreakpointCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :breakpoint_categories do |t|
      t.string   :name
      t.string   :query_param
      t.timestamps
    end
  end
end
