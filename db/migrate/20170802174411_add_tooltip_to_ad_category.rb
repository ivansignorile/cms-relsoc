class AddTooltipToAdCategory < ActiveRecord::Migration[5.0]
  def change
    add_column :ad_categories, :tooltip, :string
  end
end
