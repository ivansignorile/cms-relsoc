class AddNewPhotoToAd < ActiveRecord::Migration[5.0]
  def change
    add_column :ads, :new_photo, :boolean
  end
end
