class AddBaseCityToAd < ActiveRecord::Migration[5.0]
  def change
    add_column :ads, :base, :string
    add_column :ads, :zone, :string
  end
end
