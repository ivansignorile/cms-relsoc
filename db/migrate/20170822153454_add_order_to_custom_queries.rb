class AddOrderToCustomQueries < ActiveRecord::Migration[5.0]
  def change
    add_column :custom_queries, :order, :text
  end
end
