class CreateIssues < ActiveRecord::Migration[5.0]
  def change
    create_table :issues do |t|
      t.string :name
      t.string :email
      t.string :city
      t.string :motivation
      t.text :content

      t.timestamps
    end
  end
end
