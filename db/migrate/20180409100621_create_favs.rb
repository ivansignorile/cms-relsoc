class CreateFavs < ActiveRecord::Migration[5.0]
  def change
    create_table :favs do |t|
      t.belongs_to :user, foreign_key: true
      t.belongs_to :ad, foreign_key: true

      t.timestamps
    end
  end
end
