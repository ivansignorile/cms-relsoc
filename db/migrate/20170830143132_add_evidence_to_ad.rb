class AddEvidenceToAd < ActiveRecord::Migration[5.0]
  def change
    add_column :ads, :evidence, :boolean
  end
end
