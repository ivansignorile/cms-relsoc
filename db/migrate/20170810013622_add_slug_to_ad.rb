class AddSlugToAd < ActiveRecord::Migration[5.0]
  def change
    add_column :ads, :slug, :string, :uniq => true
  end
end
