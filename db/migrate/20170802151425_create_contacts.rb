class CreateContacts < ActiveRecord::Migration[5.0]
  def change
    create_table :contacts do |t|
      t.belongs_to :user, foreign_key: true
      t.string :whatsapp
      t.string :mobile
      t.string :email
      t.string :skype
      t.string :facebook
      t.string :twitter
      t.string :instagram
      t.string :snapchat
      t.string :website

      t.timestamps
    end
  end
end
