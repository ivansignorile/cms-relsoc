class AddAlwaysOpenToAd < ActiveRecord::Migration[5.0]
  def change
    add_column :ads, :always_open, :boolean
  end
end
