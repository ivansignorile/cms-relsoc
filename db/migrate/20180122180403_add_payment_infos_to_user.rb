class AddPaymentInfosToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :deal_from, :date
    add_column :users, :deal_to, :date
    add_column :users, :payment_method, :string
    add_column :users, :deal_notes, :text
  end
end
