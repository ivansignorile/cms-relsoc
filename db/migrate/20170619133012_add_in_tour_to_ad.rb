class AddInTourToAd < ActiveRecord::Migration[5.0]
  def change
    add_column :ads, :in_tour, :boolean
    add_column :ads, :from, :datetime
    add_column :ads, :to, :datetime
  end
end
