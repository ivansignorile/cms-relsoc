class AddTitleToAd < ActiveRecord::Migration[5.0]
  def change
    add_column :ads, :title, :text
  end
end
