class AddNoSmsNoPrivateToAd < ActiveRecord::Migration[5.0]
  def change
    add_column :ads, :no_sms_no_private, :boolean
  end
end
