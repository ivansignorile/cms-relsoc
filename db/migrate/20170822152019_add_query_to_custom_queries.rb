class AddQueryToCustomQueries < ActiveRecord::Migration[5.0]
  def change
    add_column :custom_queries, :query, :text
  end
end
