class CreateCustomQueries < ActiveRecord::Migration[5.0]
  def change
    create_table :custom_queries do |t|
      t.string :title
      t.text :path

      t.timestamps
    end
  end
end
