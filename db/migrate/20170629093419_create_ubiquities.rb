class CreateUbiquities < ActiveRecord::Migration[5.0]
  def change
    create_table :ubiquities do |t|
      t.belongs_to :ad, foreign_key: true
      t.datetime :from
      t.datetime :to

      t.timestamps
    end
  end
end
