class CreateBreakpoints < ActiveRecord::Migration[5.0]
  def change
    create_table :breakpoints do |t|
      t.belongs_to :breakpoint_category, foreign_key: true
      t.string :name

      t.timestamps
    end
  end
end
