class AddStatusToAd < ActiveRecord::Migration[5.0]
  def change
    add_column :ads, :active, :boolean, :default => true
  end
end
