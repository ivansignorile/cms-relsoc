class CreateComments < ActiveRecord::Migration[5.0]
  def change
    create_table :comments do |t|
      t.belongs_to :user, foreign_key: true
      t.belongs_to :ad, foreign_key: true
      t.text :content
      t.string :title

      t.timestamps
    end
  end
end
