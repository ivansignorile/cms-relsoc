class AddShowOnHomeToBreakpointCategory < ActiveRecord::Migration[5.0]
  def change
    add_column :breakpoint_categories, :show_on_home, :boolean, :default => false
  end
end
