class CreateAds < ActiveRecord::Migration[5.0]
  def change
    create_table :ads do |t|
      t.belongs_to :user, foreign_key: true
      t.belongs_to :ad_category, foreign_key: true
      t.string :address
      t.float :latitude
      t.float :longitude
      t.text :description
      t.integer :views_count
      t.text :queries

      t.timestamps
    end
  end
end
