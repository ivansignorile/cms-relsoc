class AddWhToAds2 < ActiveRecord::Migration[5.0]
  def change
    add_column :ads, :mhf, :integer
    add_column :ads, :mht, :integer
    add_column :ads, :thf, :integer
    add_column :ads, :tht, :integer
    add_column :ads, :whf, :integer
    add_column :ads, :wht, :integer
    add_column :ads, :tdhf, :integer
    add_column :ads, :tdht, :integer
    add_column :ads, :fhf, :integer
    add_column :ads, :fht, :integer
    add_column :ads, :shf, :integer
    add_column :ads, :sht, :integer
    add_column :ads, :sshf, :integer
    add_column :ads, :ssht, :integer
  end
end
