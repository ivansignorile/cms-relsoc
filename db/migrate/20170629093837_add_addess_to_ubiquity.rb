class AddAddessToUbiquity < ActiveRecord::Migration[5.0]
  def change
    add_column :ubiquities, :address, :string
    add_column :ubiquities, :latitude, :float
    add_column :ubiquities, :longitude, :float
  end
end
