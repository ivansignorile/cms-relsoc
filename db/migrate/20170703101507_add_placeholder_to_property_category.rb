class AddPlaceholderToPropertyCategory < ActiveRecord::Migration[5.0]
  def change
    add_column :property_categories, :placeholder, :text
  end
end
