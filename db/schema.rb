# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180409100621) do

  create_table "ad_categories", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "slug"
    t.string   "tooltip"
  end

  create_table "ads", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "user_id"
    t.integer  "ad_category_id"
    t.string   "address"
    t.float    "latitude",          limit: 24
    t.float    "longitude",         limit: 24
    t.text     "description",       limit: 65535
    t.integer  "views_count"
    t.text     "queries",           limit: 65535
    t.datetime "created_at",                                     null: false
    t.datetime "updated_at",                                     null: false
    t.boolean  "in_tour"
    t.datetime "from"
    t.datetime "to"
    t.text     "hour",              limit: 65535
    t.text     "title",             limit: 65535
    t.string   "slug"
    t.boolean  "new_photo"
    t.string   "base"
    t.string   "zone"
    t.text     "wh",                limit: 65535
    t.integer  "mhf"
    t.integer  "mht"
    t.integer  "thf"
    t.integer  "tht"
    t.integer  "whf"
    t.integer  "wht"
    t.integer  "tdhf"
    t.integer  "tdht"
    t.integer  "fhf"
    t.integer  "fht"
    t.integer  "shf"
    t.integer  "sht"
    t.integer  "sshf"
    t.integer  "ssht"
    t.boolean  "always_open"
    t.string   "secondary_base"
    t.boolean  "no_sms_no_private"
    t.boolean  "evidence"
    t.boolean  "active",                          default: true
    t.text     "notes",             limit: 65535
    t.text     "abstract",          limit: 65535
    t.index ["ad_category_id"], name: "index_ads_on_ad_category_id", using: :btree
    t.index ["user_id"], name: "index_ads_on_user_id", using: :btree
  end

  create_table "breakpoint_categories", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.string   "query_param"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.boolean  "show_on_home", default: false
  end

  create_table "breakpoint_matches", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "breakpoint_id"
    t.integer  "ad_id"
    t.integer  "breakpoint_category_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.index ["ad_id"], name: "index_breakpoint_matches_on_ad_id", using: :btree
    t.index ["breakpoint_category_id"], name: "index_breakpoint_matches_on_breakpoint_category_id", using: :btree
    t.index ["breakpoint_id"], name: "index_breakpoint_matches_on_breakpoint_id", using: :btree
  end

  create_table "breakpoints", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "breakpoint_category_id"
    t.string   "name"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.index ["breakpoint_category_id"], name: "index_breakpoints_on_breakpoint_category_id", using: :btree
  end

  create_table "cities", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.string   "prov"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "comments", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "user_id"
    t.integer  "ad_id"
    t.text     "content",    limit: 65535
    t.string   "title"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.float    "rating",     limit: 24
    t.index ["ad_id"], name: "index_comments_on_ad_id", using: :btree
    t.index ["user_id"], name: "index_comments_on_user_id", using: :btree
  end

  create_table "contacts", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "user_id"
    t.string   "whatsapp"
    t.string   "mobile"
    t.string   "email"
    t.string   "skype"
    t.string   "facebook"
    t.string   "twitter"
    t.string   "instagram"
    t.string   "snapchat"
    t.string   "website"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_contacts_on_user_id", using: :btree
  end

  create_table "custom_queries", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "title"
    t.text     "path",       limit: 65535
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.text     "query",      limit: 65535
    t.text     "order",      limit: 65535
  end

  create_table "defaults", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "field"
    t.text     "value",      limit: 65535
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "favs", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "user_id"
    t.integer  "ad_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["ad_id"], name: "index_favs_on_ad_id", using: :btree
    t.index ["user_id"], name: "index_favs_on_user_id", using: :btree
  end

  create_table "friendly_id_slugs", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
    t.index ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, length: { slug: 70, scope: 70 }, using: :btree
    t.index ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", length: { slug: 140 }, using: :btree
    t.index ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
    t.index ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree
  end

  create_table "issues", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.string   "email"
    t.string   "city"
    t.string   "motivation"
    t.text     "content",    limit: 65535
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "pictures", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "ad_id"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "attachment_file_name"
    t.string   "attachment_content_type"
    t.integer  "attachment_file_size"
    t.datetime "attachment_updated_at"
    t.integer  "order_nr"
    t.index ["ad_id"], name: "index_pictures_on_ad_id", using: :btree
  end

  create_table "properties", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "property_category_id"
    t.integer  "user_id"
    t.string   "value"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.index ["property_category_id"], name: "index_properties_on_property_category_id", using: :btree
    t.index ["user_id"], name: "index_properties_on_user_id", using: :btree
  end

  create_table "property_categories", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
    t.text     "placeholder",  limit: 65535
    t.boolean  "show_on_home",               default: false
  end

  create_table "ubiquities", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "ad_id"
    t.datetime "from"
    t.datetime "to"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.string   "address"
    t.float    "latitude",   limit: 24
    t.float    "longitude",  limit: 24
    t.index ["ad_id"], name: "index_ubiquities_on_ad_id", using: :btree
  end

  create_table "users", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "email",                                default: "",  null: false
    t.string   "encrypted_password",                   default: "",  null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                        default: 0,   null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                                         null: false
    t.datetime "updated_at",                                         null: false
    t.string   "username"
    t.text     "tel",                    limit: 65535
    t.string   "user_type",                            default: "U"
    t.date     "deal_from"
    t.date     "deal_to"
    t.string   "payment_method"
    t.text     "deal_notes",             limit: 65535
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

  add_foreign_key "ads", "ad_categories"
  add_foreign_key "ads", "users"
  add_foreign_key "breakpoint_matches", "ads"
  add_foreign_key "breakpoint_matches", "breakpoint_categories"
  add_foreign_key "breakpoint_matches", "breakpoints"
  add_foreign_key "breakpoints", "breakpoint_categories"
  add_foreign_key "comments", "ads"
  add_foreign_key "comments", "users"
  add_foreign_key "contacts", "users"
  add_foreign_key "favs", "ads"
  add_foreign_key "favs", "users"
  add_foreign_key "pictures", "ads"
  add_foreign_key "properties", "property_categories"
  add_foreign_key "properties", "users"
  add_foreign_key "ubiquities", "ads"
end
