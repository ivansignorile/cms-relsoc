require 'test_helper'

class AdCategoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ad_category = ad_categories(:one)
  end

  test "should get index" do
    get ad_categories_url
    assert_response :success
  end

  test "should get new" do
    get new_ad_category_url
    assert_response :success
  end

  test "should create ad_category" do
    assert_difference('AdCategory.count') do
      post ad_categories_url, params: { ad_category: { name: @ad_category.name } }
    end

    assert_redirected_to ad_category_url(AdCategory.last)
  end

  test "should show ad_category" do
    get ad_category_url(@ad_category)
    assert_response :success
  end

  test "should get edit" do
    get edit_ad_category_url(@ad_category)
    assert_response :success
  end

  test "should update ad_category" do
    patch ad_category_url(@ad_category), params: { ad_category: { name: @ad_category.name } }
    assert_redirected_to ad_category_url(@ad_category)
  end

  test "should destroy ad_category" do
    assert_difference('AdCategory.count', -1) do
      delete ad_category_url(@ad_category)
    end

    assert_redirected_to ad_categories_url
  end
end
