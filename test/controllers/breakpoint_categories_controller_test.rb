require 'test_helper'

class BreakpointCategoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @breakpoint_category = breakpoint_categories(:one)
  end

  test "should get index" do
    get breakpoint_categories_url
    assert_response :success
  end

  test "should get new" do
    get new_breakpoint_category_url
    assert_response :success
  end

  test "should create breakpoint_category" do
    assert_difference('BreakpointCategory.count') do
      post breakpoint_categories_url, params: { breakpoint_category: { name: @breakpoint_category.name } }
    end

    assert_redirected_to breakpoint_category_url(BreakpointCategory.last)
  end

  test "should show breakpoint_category" do
    get breakpoint_category_url(@breakpoint_category)
    assert_response :success
  end

  test "should get edit" do
    get edit_breakpoint_category_url(@breakpoint_category)
    assert_response :success
  end

  test "should update breakpoint_category" do
    patch breakpoint_category_url(@breakpoint_category), params: { breakpoint_category: { name: @breakpoint_category.name } }
    assert_redirected_to breakpoint_category_url(@breakpoint_category)
  end

  test "should destroy breakpoint_category" do
    assert_difference('BreakpointCategory.count', -1) do
      delete breakpoint_category_url(@breakpoint_category)
    end

    assert_redirected_to breakpoint_categories_url
  end
end
