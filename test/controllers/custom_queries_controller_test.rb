require 'test_helper'

class CustomQueriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @custom_query = custom_queries(:one)
  end

  test "should get index" do
    get custom_queries_url
    assert_response :success
  end

  test "should get new" do
    get new_custom_query_url
    assert_response :success
  end

  test "should create custom_query" do
    assert_difference('CustomQuery.count') do
      post custom_queries_url, params: { custom_query: { path: @custom_query.path, title: @custom_query.title } }
    end

    assert_redirected_to custom_query_url(CustomQuery.last)
  end

  test "should show custom_query" do
    get custom_query_url(@custom_query)
    assert_response :success
  end

  test "should get edit" do
    get edit_custom_query_url(@custom_query)
    assert_response :success
  end

  test "should update custom_query" do
    patch custom_query_url(@custom_query), params: { custom_query: { path: @custom_query.path, title: @custom_query.title } }
    assert_redirected_to custom_query_url(@custom_query)
  end

  test "should destroy custom_query" do
    assert_difference('CustomQuery.count', -1) do
      delete custom_query_url(@custom_query)
    end

    assert_redirected_to custom_queries_url
  end
end
