require 'test_helper'

class BreakpointsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @breakpoint = breakpoints(:one)
  end

  test "should get index" do
    get breakpoints_url
    assert_response :success
  end

  test "should get new" do
    get new_breakpoint_url
    assert_response :success
  end

  test "should create breakpoint" do
    assert_difference('Breakpoint.count') do
      post breakpoints_url, params: { breakpoint: { breakpoint_category_id: @breakpoint.breakpoint_category_id, name: @breakpoint.name } }
    end

    assert_redirected_to breakpoint_url(Breakpoint.last)
  end

  test "should show breakpoint" do
    get breakpoint_url(@breakpoint)
    assert_response :success
  end

  test "should get edit" do
    get edit_breakpoint_url(@breakpoint)
    assert_response :success
  end

  test "should update breakpoint" do
    patch breakpoint_url(@breakpoint), params: { breakpoint: { breakpoint_category_id: @breakpoint.breakpoint_category_id, name: @breakpoint.name } }
    assert_redirected_to breakpoint_url(@breakpoint)
  end

  test "should destroy breakpoint" do
    assert_difference('Breakpoint.count', -1) do
      delete breakpoint_url(@breakpoint)
    end

    assert_redirected_to breakpoints_url
  end
end
