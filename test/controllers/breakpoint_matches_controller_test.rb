require 'test_helper'

class BreakpointMatchesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @breakpoint_match = breakpoint_matches(:one)
  end

  test "should get index" do
    get breakpoint_matches_url
    assert_response :success
  end

  test "should get new" do
    get new_breakpoint_match_url
    assert_response :success
  end

  test "should create breakpoint_match" do
    assert_difference('BreakpointMatch.count') do
      post breakpoint_matches_url, params: { breakpoint_match: { ad_id: @breakpoint_match.ad_id, breakpoint_category_id: @breakpoint_match.breakpoint_category_id, breakpoint_id: @breakpoint_match.breakpoint_id } }
    end

    assert_redirected_to breakpoint_match_url(BreakpointMatch.last)
  end

  test "should show breakpoint_match" do
    get breakpoint_match_url(@breakpoint_match)
    assert_response :success
  end

  test "should get edit" do
    get edit_breakpoint_match_url(@breakpoint_match)
    assert_response :success
  end

  test "should update breakpoint_match" do
    patch breakpoint_match_url(@breakpoint_match), params: { breakpoint_match: { ad_id: @breakpoint_match.ad_id, breakpoint_category_id: @breakpoint_match.breakpoint_category_id, breakpoint_id: @breakpoint_match.breakpoint_id } }
    assert_redirected_to breakpoint_match_url(@breakpoint_match)
  end

  test "should destroy breakpoint_match" do
    assert_difference('BreakpointMatch.count', -1) do
      delete breakpoint_match_url(@breakpoint_match)
    end

    assert_redirected_to breakpoint_matches_url
  end
end
