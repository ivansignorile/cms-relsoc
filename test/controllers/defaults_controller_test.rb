require 'test_helper'

class DefaultsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @default = defaults(:one)
  end

  test "should get index" do
    get defaults_url
    assert_response :success
  end

  test "should get new" do
    get new_default_url
    assert_response :success
  end

  test "should create default" do
    assert_difference('Default.count') do
      post defaults_url, params: { default: { field: @default.field, value: @default.value } }
    end

    assert_redirected_to default_url(Default.last)
  end

  test "should show default" do
    get default_url(@default)
    assert_response :success
  end

  test "should get edit" do
    get edit_default_url(@default)
    assert_response :success
  end

  test "should update default" do
    patch default_url(@default), params: { default: { field: @default.field, value: @default.value } }
    assert_redirected_to default_url(@default)
  end

  test "should destroy default" do
    assert_difference('Default.count', -1) do
      delete default_url(@default)
    end

    assert_redirected_to defaults_url
  end
end
