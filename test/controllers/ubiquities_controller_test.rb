require 'test_helper'

class UbiquitiesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ubiquity = ubiquities(:one)
  end

  test "should get index" do
    get ubiquities_url
    assert_response :success
  end

  test "should get new" do
    get new_ubiquity_url
    assert_response :success
  end

  test "should create ubiquity" do
    assert_difference('Ubiquity.count') do
      post ubiquities_url, params: { ubiquity: { ad_id: @ubiquity.ad_id, from: @ubiquity.from, to: @ubiquity.to } }
    end

    assert_redirected_to ubiquity_url(Ubiquity.last)
  end

  test "should show ubiquity" do
    get ubiquity_url(@ubiquity)
    assert_response :success
  end

  test "should get edit" do
    get edit_ubiquity_url(@ubiquity)
    assert_response :success
  end

  test "should update ubiquity" do
    patch ubiquity_url(@ubiquity), params: { ubiquity: { ad_id: @ubiquity.ad_id, from: @ubiquity.from, to: @ubiquity.to } }
    assert_redirected_to ubiquity_url(@ubiquity)
  end

  test "should destroy ubiquity" do
    assert_difference('Ubiquity.count', -1) do
      delete ubiquity_url(@ubiquity)
    end

    assert_redirected_to ubiquities_url
  end
end
