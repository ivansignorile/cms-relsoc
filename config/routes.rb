Rails.application.routes.draw do
  resources :favs
  resources :comments
  resources :issues, :path => "contatti" do
      collection do
         get "confirmation"
      end
  end
  resources :cities
  resources :custom_queries
  resources :contacts
  resources :properties
  resources :property_categories
  resources :defaults
  resources :ubiquities
  resources :pictures
  resources :breakpoint_matches

  resources :ads, :path => "annunci" do
      member do
        get "increase"
        get "pictures"
        get "toggle"
        get "pay"
      end
      collection do
        get "admin"
        get "cities"
        get "set_ads_order"
        get "by_zone"
        get "setup"
      end
  end

  resources :ads, :path => "accompagnatrici", only: [:show]



  resources :breakpoints
  resources :ad_categories, :path => "pages" do
      collection do
      get "disclaimer"
      get "infos"
      end
  end
  resources :breakpoint_categories
  devise_for :users

  resources :users do
      member do
        get "show"
        get "guest"
      end
      collection do
        get "properties"
        get "index"
        post "update_user"
        put "up"
        get "profile"
        get "infos"
      end
  end

  root "ads#index"

  scope "/(:category)" do
    scope "/(:city)" do
      resources :ads, only: [:index], :path => "annunci"
    end
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
